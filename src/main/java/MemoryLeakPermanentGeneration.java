import javassist.ClassPool;

public class MemoryLeakPermanentGeneration {
    static ClassPool classPool = ClassPool.getDefault();

    public static void main(String[] args) throws Exception {
        for (double i = 1; i < Integer.MAX_VALUE; i++) {
            Class clazz = classPool.makeClass(String.format("%.1f outOfMemory.MetaSpace| %.3f%% complete", i,
                    (i / Integer.MAX_VALUE) * 100.000 )).toClass();
            if (i%1000 == 0){
                System.out.println(clazz.getName());
            }

        }
    }
}
