import java.util.LinkedList;
import java.util.List;

public class MemoryLeak {
    public static void main(String[] args) {

        List<Object[]> list = new LinkedList<>();
        for (long i = 0; ; i++) {
            list.add(new Object[100]);
            if (i % (Integer.MAX_VALUE/2) == 0){
                list = new LinkedList<>();
            }
        }
    }
}
